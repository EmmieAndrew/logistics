<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserDetail;
use Response;
use Validator;
use DB;
use Hash;
use Crypt;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

class CustomerAPIController extends Controller
{
        public function login(Request $request){
        	// dd("nnnn");
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else{
            $existingUser = User::where('email',$request->email)
            ->orwhere('phone_no',$request->email)
            ->where('ban','0')
            ->first();
            if($existingUser){
                if(Hash::check($request->password , $existingUser['password'])){
                    return response()->json([
                        'status' => '1',
                        'message'=>'Logged in successfully', 
                        'user_id'=>(String)$existingUser->id,
                        'username'=>$existingUser->username,
                        'email'=>$existingUser->email,
                        'token'=>$existingUser->remember_token
                    ], $this->successStatus);
                }else {
                  return response()->json(['message'=>'Email or Password is incorrect','status'=>'0'], $this->successStatus);
                }
            }else {
                return response()->json(['message'=>'User not found','status'=>'0'], $this->successStatus);
            }
        }
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users,username',
            'email' => 'bail|required|email|unique:users,email',
            'password'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else{
            $userExists = User::where('email',$request->email)->first();
            if ($userExists) {
                return response()->json([
                    'message'=>'Email Already Exists!!',
                    'status'=>'1',
                ], $this->successStatus); 
            }else{
                $newUser =  new \App\Models\User;
                $newUser->username = $request->username;
                $newUser->phone_no = $request->phone_no;
                $newUser->email = $request->email; 
                $newUser->password = $request->password;
                // $newUser->dob = $request->dob;
                $token = (string)mt_rand(100,1000000);
                $token_en = Crypt::encrypt($token);
                $newUser->remember_token = $token_en;
                $newUser->device_id = $request->device_id;
                $newUser->device_type = $request->device_type;
                $newUser->save();
                return response()->json([
                    'status'=>'1',
                    'message'=>'User Registered successfully',
                    'user_id'=>(String)$newUser->id,
                    'token'=>$newUser->remember_token
                ], $this->successStatus);
            }
        }
    }

    public function socialLogin(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [
            'social_id' => 'required',
            'username' =>'required',
            'image' => 'required', #url of image from gmail or fb
            'email' =>'required',
            'login_type' =>'required' #G OR F
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            $userExist = User::where('social_id',$saveArray['social_id'])->first();
            if($userExist){
                $checkEmail = User::where('email',$saveArray['email'])->first();
                if ($checkEmail) {
                    return response()->json([
                        'message'=>'Email Already Exists!!',
                        'status'=>'1',
                        'remember_token' => $checkEmail['remember_token'],
                        'user_id'=>(String)$checkEmail['id'],
                    ], $this->successStatus);
                }
                $checkUsername = User::where('username',$saveArray['username'])->first();
                if ($checkUsername) {
                    return response()->json([
                        'message'=>'Username Already Exists!!',
                        'status'=>'1',
                        'remember_token' => $checkUsername['remember_token'],
                        'user_id'=>(String)$checkUsername['id'],
                    ], $this->successStatus);
                }
                $updateData = [
                    'device_id'=>$saveArray['device_id']?$saveArray['device_id']:$userExist['device_id'],
                    'device_type'=>$saveArray['device_type']?$saveArray['device_type']:$userExist['device_type'],
                    'login_type'=>$saveArray['login_type']?$saveArray['login_type']:$userExist['login_type'],
                    'social_id'=>$saveArray['social_id']?$saveArray['social_id']:$userExist['social_id'],
                    'name' => $saveArray['name']?$saveArray['name']:$userExist['name'],
                    // 'username' => $saveArray['username']?$saveArray['username']:$userExist['username'],
                    // 'email' => $saveArray['email']?$saveArray['email']:$userExist['email'],
                    'image'=>$saveArray['image']?$saveArray['image']:$userExist['photo'],
                    'updated_at'=>date('Y-m-d h:i:s')
                ];
                $update = User::where('social_id',$saveArray['social_id'])->update($updateData);
                if ($update) {
                    return response()->json([
                        'message'=>'login successfully',
                        'status'=>'1',
                        'user_id'=>(String)$userExist['id'],
                        'token'=>$userExist->remember_token,
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'login Failed. Please try again later.',
                        'status'=>'0'
                    ], $this->successStatus);
                }
            }else{
                $checkEmail = User::where('email',$saveArray['email'])->first();
                if ($checkEmail) {
                    return response()->json([
                        'message'=>'Email Already Exists!!',
                        'status'=>'1',
                        'remember_token' => $checkEmail['remember_token'],
                        'user_id'=>(String)$checkEmail['id'],
                    ], $this->successStatus);
                }
                $checkUsername = User::where('username',$saveArray['username'])->first();
                if ($checkUsername) {
                    return response()->json([
                        'message'=>'Username Already Exists!!',
                        'status'=>'1',
                        'remember_token' => $checkUsername['remember_token'],
                        'user_id'=>(String)$checkUsername['id'],
                    ], $this->successStatus);
                }
                $user = [
                    'device_id'=>$saveArray['device_id']?$saveArray['device_id']:'',
                    'device_type'=>$saveArray['device_type']?$saveArray['device_type']:'',
                    'social_id'=>$saveArray['social_id'],
                    'login_type'=>$saveArray['login_type'],
                    'username' => $saveArray['username'],
                    'name' => $saveArray['name'],
                    'phone_no' => '',
                    'password'=>'',
                    'email' => $saveArray['email'],
                    'image'=>$saveArray['image'],
                    'remember_token' => '',
                    'created_at' => date('Y-m-d h:i:s')
                ];
                $get_user_id =  User::insertGetId($user);
                if (!empty($get_user_id)) {
                    $random = (string)mt_rand(10,1000000);
                    $token = (string)$get_user_id.$random;
                    $token_en = Crypt::encrypt($token);
                    $update_token = [
                        'remember_token' => $token_en,
                        'updated_at' => date('Y-m-d h:i:s')
                    ];
                    $update = User::where(['id'=>$get_user_id])->update($update_token);
                    if($update){
                        return response()->json([
                            'message'=>'Registered successfully',
                            'status'=>'1',
                            'user_id'=>(String)$get_user_id,
                            'token'=>$token_en,
                        ], $this->successStatus);
                    }
                }
                else{
                    return response()->json([
                        'message'=>'Signup Failed. Please try again later.',
                        'status'=>'1'
                    ], $this->successStatus);
                }
            }
        }
    }

    public function getOwnProfile(Request $request){
        try{
            $token_de = $request->header('token');
        } catch (DecryptException $e){
            return response()->json(['status' => '0', 'message' => $e]);
        }
        $existingUser = User::where(['remember_token'=>$token_de])->first();
        if($existingUser){
            return response()->json([
                'status'=>'1',
                'message'=>'User fetched successfully',
                'username' => $existingUser->username,
                // 'name' => $existingUser->name,
                'email' => $existingUser->email,
                // 'phone_no' => $existingUser->phone_no,
                // 'DOB' => $existingUser->DOB,
                // 'gender' => $existingUser->gender,
                'followers' => '',
                'following' => '',
                'bio' => '',
                'image' => $image,
            ]);
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function editProfile(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = User::where('remember_token', $token)->first();
        if($userExist){
            if($request->hasFile('image')){
                $image= time().$request->image->getClientOriginalName();
                $request->image->move(public_path('/user_images') . '/', $image);
            }else{
                $image = $userExist['image'];
            }
            $email = '';
            if (!empty($saveArray['email'])) {
                $checkEmail = User::where('email',$saveArray['email'])->first();
                if ($checkEmail) {
                    return response()->json(['status'=>'0','message'=>'Email already exists'
                    ], $this->successStatus);
                }else {
                    $email = $saveArray['email'];
                }
            }
            $phone_no = '';
            if (!empty($saveArray['phone_no'])) {
                $checkPhone = User::where('phone_no',$saveArray['phone_no'])->first();
                if ($checkPhone) {
                    return response()->json(['status'=>'1','message'=>'Phone already exists'
                    ], $this->successStatus);
                }else {
                    $phone_no = $saveArray['phone_no'];
                }
            }
            $updateProfile = [
                'username'=>$saveArray['username']?$saveArray['username']:$userExist['username'],
                'name'=>$saveArray['name']?$saveArray['name']:$userExist['name'],
                'gender'=>$saveArray['gender']?$saveArray['gender']:$userExist['gender'],
                'dob'=>$saveArray['dob']?$saveArray['dob']:$userExist['dob'],
                'email'=>$email?$email:$userExist['email'],
                'phone_no'=>$phone_no?$phone_no:$userExist['phone_no'],
                'image'=>$image,
                'updated_at'=>date('Y-m-d h:i:s')
            ];
            $update = User::where(['remember_token'=>$token])->update($updateProfile);
            if($update){
                $userDetails = User::where(['remember_token'=>$token])->first();
                // $image = asset('/user_images/' . $userDetails->image);
                return response()->json([
                    'status'=>'1',
                    'message'=>'User Updated Successfully',
                    'user_details'=>$userDetails,
                ], $this->successStatus);
            }
            else{
                return response()->json(['message'=>'User does not Update','status'=>'0'], $this->badrequest);
            }
        }else{
            return response()->json(['message'=>'User does not exists','status'=>'0'], $this->badrequest);
        }
    }

    public function forgotPassword(Request $request){
        $saveArray = $request->all();
        if (User::where('email', '=', $saveArray['email'])->count() > 0) {
            $new_password= $this->randomPassword();
            $new_password_en = Hash::make($new_password);
            $update_password = [
                'password' => $new_password_en,
                'updated_at' => date('Y-m-d h:i:s')
            ];
            $update = User::where(['email'=>$saveArray['email']])->update($update_password);
            if($update){
                $name = $saveArray['email'];
                $msg = [
                    "name" => $name,
                    "new_password" => $new_password
                ];
                $email = $saveArray['email'];
                Mail::send('mail', $msg, function($message) use ($email) {
                    $message->to($email);
                    $message->subject('SocialApp');
                });
                return response()->json(['status' =>'1' ,'message'=>'New Password has been mailed to you. Please check your email'], $this->successStatus);
            }
            // $response = $this->broker()->sendResetLink(
            //     $request->only('email')
            // );
            // if($response == 'passwords.sent'){
            //     $response = 'We have e-mailed your password reset link!';
            //     return response()->json(['status' =>'1' ,'message'=>'success', 'data'=> $response], $this->successStatus);
            // } else{
            //     $response = 'Your password reset email not sent!';
            //     return response()->json(['status' =>'0' ,'message'=>'error', 'data'=> $response], $this->successStatus);
            // }
        } else{
            return response()->json(['status' =>'error' ,'message'=>'Unauthorised'], 401);
        }
    }

    public function changePassword(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $existingUser = User::where('remember_token', $token)->first();
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
          ]);
          if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
          }
          elseif ($request->old_password == $request->new_password) {
            return response()->json(['message'=>'New password is same as old password','status'=>'0'], $this->successStatus);
          }
          else{
              if(Hash::check($request->old_password, $existingUser->password)){
                $password_en =  Hash::make($request->new_password);
                $update = User::where(['id'=>$existingUser->id])->update(['password' => $password_en]);
                // echo $update;
                if($update){
                  return response()->json([
                        'status'=>'1',
                        'message'=>'Password changed Successfully'
                      ], $this->successStatus);
                }
                else{
                  return response()->json([
                        'status'=>'0',
                        'message'=>'Error on changing password'
                      ], $this->successStatus);
                }
              }
              else{
                return response()->json([
                    'status'=>'0',
                    'message'=>'Old password is incorrect'
                ], $this->successStatus);
            }
        }
    }
}
