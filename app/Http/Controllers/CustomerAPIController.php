<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserDetail;
use App\Models\User;
use App\Models\Truck;
use App\Models\Book;
use App\Models\Favourite;
use App\Models\Follow;
use App\Models\Driver;
use App\Models\Rating;
use App\Models\Message;
use Carbon;
use DateTime;
use Response;
use Validator;
use DB;
use Hash;
use Crypt;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

class CustomerAPIController extends Controller
{

    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;


        public function login(Request $request){
        	// dd("ff");
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else{
            $existingUser = UserDetail::where('email',$request->email)
            ->orWhere('phone_no',$request->email)
            ->first();
            // print_r($existingUser);die();
            if($existingUser){
                if($request->password == $existingUser['plain_password']){
                    return response()->json([
                        'status' => '1',
                        'message'=>'Logged in successfully', 
                        'user_id'=>(String)$existingUser->id,
                        'username'=>$existingUser->username,
                        'email'=>$existingUser->email,
                        'type' =>$existingUser->type,
                        'token'=>$existingUser->remember_token
                    ], $this->successStatus);
                }else {
                  return response()->json(['message'=>'Email or Password is incorrect','status'=>'0'], $this->successStatus);
                }
            }else {
                return response()->json(['message'=>'User not found','status'=>'0'], $this->successStatus);
            }
        }
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'bail|required|email|unique:users,email',
            'password'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else{
            $userExists = UserDetail::where('email',$request->email)->first();
            if ($userExists) {
                return response()->json([
                    'message'=>'Email Already Exists!!',
                    'status'=>'1',
                ], $this->successStatus); 
            }
            $phoneExists = UserDetail::where('phone_no',$request->phone_no)->first();
            if ($phoneExists) {
                return response()->json([
                    'message'=>'Phone Already Exists!!',
                    'status'=>'1',
                ], $this->successStatus); 
            }
            // else{
                $space = ' ';
                $newUser =  new \App\Models\UserDetail;
                $newUser->name = $request->first_name.$space.$request->last_name;
                $newUser->phone_no = $request->phone_no;
                $newUser->email = $request->email; 
                $newUser->type = $request->type;
                $newUser->password = Hash::make($request->password);
                $newUser->plain_password = $request->password;
                $newUser->gender = '';
                $token = (string)mt_rand(100,1000000);
                $token_en = Crypt::encrypt($token);
                $newUser->remember_token = $token_en;
                $newUser->device_id = $request->device_id;
                $newUser->device_type = $request->device_type;
                $newUser->save();
                return response()->json([
                    'status'=>'1',
                    'message'=>'User Registered successfully',
                    'user_id'=>(String)$newUser->id,
                    'token'=>$newUser->remember_token
                ], $this->successStatus);
            // }
        }
    }

    public function socialLogin(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [
            'social_id' => 'required',
            'name' =>'required',
            'image' => 'required', #url of image from gmail or fb
            'email' =>'required',
            'login_type' =>'required' #G OR F
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            $userExist = UserDetail::where('social_id',$saveArray['social_id'])->first();
            if($userExist){
                $updateData = [
                    'device_id'=>$saveArray['device_id']?$saveArray['device_id']:$userExist['device_id'],
                    'device_type'=>$saveArray['device_type']?$saveArray['device_type']:$userExist['device_type'],
                    'login_type'=>$saveArray['login_type']?$saveArray['login_type']:$userExist['login_type'],
                    'social_id'=>$saveArray['social_id']?$saveArray['social_id']:$userExist['social_id'],
                    'name' => $saveArray['name']?$saveArray['name']:$userExist['name'],
                    'login_type' => $saveArray['login_type']?$saveArray['login_type']:$userExist['login_type'],
                    // 'email' => $saveArray['email']?$saveArray['email']:$userExist['email'],
                    'image'=>$saveArray['image']?$saveArray['image']:$userExist['photo'],
                    'updated_at'=>date('Y-m-d h:i:s')
                ];
                $update = UserDetail::where('social_id',$saveArray['social_id'])->update($updateData);
                if ($update) {
                    return response()->json([
                        'message'=>'login successfully',
                        'status'=>'1',
                        'user_id'=>(String)$userExist['id'],
                        'token'=>$userExist->remember_token,
                    ], $this->successStatus);
                }
                else{
                    return response()->json([
                        'message'=>'login Failed. Please try again later.',
                        'status'=>'0'
                    ], $this->successStatus);
                }
            }else{
                $checkEmail = UserDetail::where('email',$saveArray['email'])->first();
                if ($checkEmail) {
                    return response()->json([
                        'message'=>'Email Already Exists!!',
                        'status'=>'1',
                        'remember_token' => $checkEmail['remember_token'],
                        'user_id'=>(String)$checkEmail['id'],
                    ], $this->successStatus);
                }
                $user = [
                    'device_id'=>$saveArray['device_id']?$saveArray['device_id']:'',
                    'device_type'=>$saveArray['device_type']?$saveArray['device_type']:'',
                    'social_id'=>$saveArray['social_id'],
                    'login_type'=>$saveArray['login_type'],
                    // 'username' => $saveArray['username'],
                    'name' => $saveArray['name'],
                    'phone_no' => '',
                    'password'=>'',
                    'email' => $saveArray['email'],
                    'image'=>$saveArray['image'],
                    'remember_token' => '',
                    'created_at' => date('Y-m-d h:i:s')
                ];
                $get_user_id =  UserDetail::insertGetId($user);
                if (!empty($get_user_id)) {
                    $random = (string)mt_rand(10,1000000);
                    $token = (string)$get_user_id.$random;
                    $token_en = Crypt::encrypt($token);
                    $update_token = [
                        'remember_token' => $token_en,
                        'updated_at' => date('Y-m-d h:i:s')
                    ];
                    $update = UserDetail::where(['id'=>$get_user_id])->update($update_token);
                    if($update){
                        return response()->json([
                            'message'=>'Registered successfully',
                            'status'=>'1',
                            'user_id'=>(String)$get_user_id,
                            'token'=>$token_en,
                        ], $this->successStatus);
                    }
                }
                else{
                    return response()->json([
                        'message'=>'Signup Failed. Please try again later.',
                        'status'=>'1'
                    ], $this->successStatus);
                }
            }
        }
    }

    public function getOwnProfile(Request $request){
        try{
            $token_de = $request->header('token');
        } catch (DecryptException $e){
            return response()->json(['status' => '0', 'message' => $e]);
        }
        $existingUser = UserDetail::where(['remember_token'=>$token_de])->first();
        if($existingUser){
            $image = asset('/user_images/' . $existingUser->image);

            return response()->json([
                'status'=>'1',
                'message'=>'User fetched successfully',
                'first_name' => $existingUser->name,
                'last_name' => $existingUser->name,
                'location' => $existingUser->location,
                'email' => $existingUser->email,
                'phone_no' => $existingUser->phone_no,
                // 'DOB' => $existingUser->DOB,
                // 'gender' => $existingUser->gender,
                // 'followers' => '',
                // 'following' => '',
                'bio' => '',
                'image' => $image,
            ]);
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function editProfile(Request $request){
        $saveArray = $request->all();
        $token = $request->header('token');
        $userExist = UserDetail::where('remember_token', $token)->first();
        if($userExist){
            if($request->hasFile('image')){
                $image= time().$request->image->getClientOriginalName();
                $request->image->move(public_path('/user_images') . '/', $image);
            }else{
                $image = $userExist['image'];
            }
            $email = '';
            if (!empty($saveArray['email'])) {
                $checkEmail = UserDetail::where('email',$saveArray['email'])->first();
                if ($checkEmail) {
                    return response()->json(['status'=>'0','message'=>'Email already exists'
                    ], $this->successStatus);
                }else {
                    $email = $saveArray['email'];
                }
            }
            // $phone_no = '';
            // if (!empty($saveArray['phone_no'])) {
            //     $checkPhone = UserDetail::where('phone_no',$saveArray['phone_no'])->first();
            //     if ($checkPhone) {
            //         return response()->json(['status'=>'1','message'=>'Phone already exists'
            //         ], $this->successStatus);
            //     }else {
            //         $phone_no = $saveArray['phone_no'];
            //     }
            // }
            $updateProfile = [
                'name'=>$saveArray['first_name']?$saveArray['first_name']:$userExist['name'],
                'name'=>$saveArray['last_name']?$saveArray['last_name']:$userExist['name'],
                'gender'=>$saveArray['gender']?$saveArray['gender']:$userExist['gender'],
                'location'=>$saveArray['location']?$saveArray['location']:$userExist['location'],
                // 'email'=>$email?$email:$userExist['email'],
                'phone_no'=>$saveArray['phone_no']?$saveArray['phone_no']:$userExist['phone_no'],
                'image'=>$image,
                'updated_at'=>date('Y-m-d h:i:s')
            ];
            $update = UserDetail::where(['remember_token'=>$token])->update($updateProfile);
            if($update){
                $userDetails = UserDetail::where(['remember_token'=>$token])->first();
                // $image = asset('/user_images/' . $userDetails->image);
                return response()->json([
                    'status'=>'1',
                    'message'=>'User Updated Successfully',
                    'user_details'=>$userDetails,
                ], $this->successStatus);
            }
            else{
                return response()->json(['message'=>'User does not Update','status'=>'0'], $this->badrequest);
            }
        }else{
            return response()->json(['message'=>'User does not exists','status'=>'0'], $this->badrequest);
        }
    }

    public function randomPassword($length = 8) 
    {
      $characters = '23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }

    public function generateOTP($length = 4){
        $otp = mt_rand(1000,9999);
        return $otp;
    }

    public function forgotPassword(Request $request){
        $saveArray = $request->all();
        if (UserDetail::where('email', '=', $saveArray['email'])->count() > 0) {
            $new_password= $this->generateOTP();
            // $new_password_en = Hash::make($new_password);
            // $update_password = [
            //     'password' => $new_password_en,
            //     'updated_at' => date('Y-m-d h:i:s')
            // ];
            $update = UserDetail::where(['email'=>$saveArray['email']]);
            if($update){
                $name = $saveArray['email'];
                $msg = [
                    "name" => $name,
                    "new_password" => $new_password
                ];
                $email = $saveArray['email'];
                //dd($email);
                Mail::send('mail', $msg, function($message) use ($email) {
                    $message->to($email);
                    $message->subject('LogisticApp');
                });
                return response()->json(['status' =>'1' ,'generated_otp'=>$new_password,'message'=>'New otp has been mailed to you. Please check your email'], $this->successStatus);
            }
            // $response = $this->broker()->sendResetLink(
            //     $request->only('email')
            // );
            // if($response == 'passwords.sent'){
            //     $response = 'We have e-mailed your password reset link!';
            //     return response()->json(['status' =>'1' ,'message'=>'success', 'data'=> $response], $this->successStatus);
            // } else{
            //     $response = 'Your password reset email not sent!';
            //     return response()->json(['status' =>'0' ,'message'=>'error', 'data'=> $response], $this->successStatus);
            // }
        } else{
            return response()->json(['status' =>'error' ,'message'=>'Unauthorised'], 401);
        }
    }

    // public function changePassword(Request $request){
    //     $saveArray = $request->all();
    //     $token = $request->header('token');
    //     $existingUser = UserDetail::where('remember_token', $token)->first();
    //     $validator = Validator::make($request->all(), [
    //         'old_password' => 'required',
    //         'new_password' => 'required',
    //       ]);
    //       if ($validator->fails()) {
    //         return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
    //       }
    //       elseif ($request->old_password == $request->new_password) {
    //         return response()->json(['message'=>'New password is same as old password','status'=>'0'], $this->successStatus);
    //       }
    //       else{
    //           if(Hash::check($request->old_password, $existingUser->password)){
    //             $password_en =  Hash::make($request->new_password);
    //             $update = UserDetail::where(['id'=>$existingUser->id])->update(['password' => $password_en]);
    //             // echo $update;
    //             if($update){
    //               return response()->json([
    //                     'status'=>'1',
    //                     'message'=>'Password changed Successfully'
    //                   ], $this->successStatus);
    //             }
    //             else{
    //               return response()->json([
    //                     'status'=>'0',
    //                     'message'=>'Error on changing password'
    //                   ], $this->successStatus);
    //             }
    //           }
    //           else{
    //             return response()->json([
    //                 'status'=>'0',
    //                 'message'=>'Old password is incorrect'
    //             ], $this->successStatus);
    //         }
    //     }
    // }

    //Truck API's

    public function driverInfo(Request $request){
        try{
            $driver_id = $request->driver_id;
        } catch (DecryptException $e){
            return response()->json(['status' => '0', 'message' => $e]);
        }
        $existingUser = $driver = Driver::where('id', $driver_id)->first();
        if($existingUser){
            $image = asset('/images/' . $existingUser->image);
            return response()->json([
                'status'=>'1',
                'message'=>'User fetched successfully',
                'name' => $existingUser->name,
                'address' => $existingUser->address,
                'driving_license' => $existingUser->driving_license,
                'email' => $existingUser->email,
                'vehicle_no' => $existingUser->vehicle_no,
                'vehicle_type' => $existingUser->vehicle_type,
                'followers' => rand(1,55),
                'ratings' => rand(1,5),
                'is_follow' => rand(0,1),
                'image' => $image,
            ]);
        }
        else{
            return response()->json(['message'=>'Unauthorised','status'=>'0'], $this->unauthorised);
        }
    }

    public function getlist(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($saveArray, ['type' => 'required']);
        if ($validator->fails())
        {
            return response()
                ->json(['message' => $validator->errors()
                ->first() , 'status' => '0']);
        }
        else
        {
        $gettrucklist = Truck::where(['type' =>$saveArray['type']])->get();
        if ($gettrucklist)
        {
           $gettrucklistsArr = [];
       foreach($gettrucklist as $gettruck){
        $driver = Driver::where('vehicle_no', '1')
        ->first();
               $listing['truck_id'] = $gettruck['id'];
               $listing['truck_name'] = 'Mahindra';
               $listing['transport'] = 'Mahindra';
               $listing['km'] = '250';
               $listing['phone '] = '01784154285';
               $listing['payload'] = $gettruck['payload'];
               $listing['cargobox'] =$gettruck['cargobox'];
               $listing['type'] =$gettruck['type'];
               $listing['truck_type'] =$gettruck['truck_type'];
               $listing['vehicle_no'] =$gettruck['vehicle_no'];
               $listing['driver_name'] =$driver['name']?$driver['name']:'';
               $listing['profile_pic'] =url('/user_images', 'default.jpg');
               $listing['phone_num'] =$driver['phone_num'];
               $listing['images'] = url('/user_images', 'default.jpg');
               $listing['driver_id'] = '1';
               $listing['followers'] = rand(1,55);
               $listing['ratings'] = rand(1,5);
               $listing['is_follow'] = rand(0,1);
               $listing['km'] = '250';
               $listing['phone '] = '01784154285';

               $gettrucklistsArr[] = $listing;
            }
            return response()->json(['status' => '1', 'message' => 'Data retrieved successfully','truck_listing' => $gettrucklistsArr]);
        }
            else
            {
                return response()->json(['status' => '0', 'message' => 'no data', ]);
            }
        }
    }

    public function addbooking(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(),[
            'user_id'=>'required',
            'date' => 'required',
            'book_from'=>'required',
            'book_to' => 'required',
            'truck_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else
        {
            $data1 = array(
                'user_id' => $saveArray['user_id'],
                'truck_id' => $saveArray['truck_id'],
                'book_from' => $saveArray['book_from'],
                'book_to' => $saveArray['book_to']
             );  
             $booktruck = Book::insert($data1);
             if ($booktruck){
                $allTrucks = Truck::all();
                if (!empty($allTrucks->device_id)) {
                    $this->notifications($getType,$usertype);
                }
                return response()->json(['status' => '1', 'message' => 'Booked succesfully']);
            }
            else
            {
                return response()->json(['status' => '1', 'message' => 'something went wrong']);
            }    
        }   

    }

    public function sendMessage(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(),[
            'sender_id'=>'required',
            'reciever_id' => 'required',
            'message' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else
        {
            $data = array(
                'sender_id' => $saveArray['sender_id'],
                'reciever_id' => $saveArray['reciever_id'],
                'message' => $saveArray['message']
             );  
             $rate_driver = Message::insert($data);
             if ($rate_driver){
                return response()->json(['status' => '1', 'message' => 'Message sent succesfully']);
            }
            else
            {
                return response()->json(['status' => '1', 'message' => 'something went wrong']);
            }    
        }   
    }

    public function getMessages(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(),[
            'sender_id'=>'required',
            'reciever_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else{
            $messageArr = [];
            $get_messages = Message::where('sender_id',$request->sender_id)
            ->where('reciever_id',$request->reciever_id)
            ->orWhere('sender_id',$request->reciever_id)
            ->where('reciever_id',$request->sender_id)
            ->orderBy('id','DESC')
            ->get();
            // $lastMessage = Message::where('sender_id',$request->sender_id)
            // ->where('reciever_id',$request->reciever_id)
            // ->orWhere('sender_id',$request->reciever_id)
            // ->where('reciever_id',$request->sender_id)
            // ->orderBy('id','DESC')
            // ->first();
            // print_r(json_encode($lastMessage));die();
            foreach($get_messages as $message){
                $sender = UserDetail::where('id', $message->sender_id)->first();
                $reciever = UserDetail::where('id', $message->reciever_id)->first();
                $listing['id'] = $message['id'];
                $listing['sender_id'] = $message->sender_id;
                // $listing['sender'] = $sender;
                $listing['reciever_id'] = $message->reciever_id;
                // $listing['reciever'] = $reciever;
                $listing['message'] = $message->message;
                $listing['created_at'] = $message->created_at;
                $messageArr[] = $listing;
            }
            return response()->json(['status' => '1', 'message' => 'Data retrieved successfully','messages' => $messageArr]);
        }
    }

    public function recentChats(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(),[
            'user_id'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else{
            $messageArr = [];
            $get_messages = Message::where('sender_id',$request->user_id)
            ->orWhere('reciever_id',$request->user_id)
            ->orderBy('id','DESC')
            ->get();
            
            // print_r(json_encode($get_messages));die();
            foreach($get_messages as $message){
                $lastMessage = Message::where('sender_id',$message->sender_id)
                ->where('reciever_id',$message->reciever_id)
                ->orWhere('sender_id',$message->reciever_id)
                ->where('reciever_id',$message->sender_id)
                ->orderBy('id','DESC')
                ->first();
                $sender = UserDetail::where('id', $message->sender_id)->first();
                $reciever = UserDetail::where('id', $message->reciever_id)->first();
                $listing['id'] = $message['id'];
                $listing['sender_id'] = $message->sender_id;
                $listing['sender'] = $sender;
                $listing['reciever_id'] = $message->reciever_id;
                $listing['reciever'] = $reciever;
                $listing['lastMessage'] = $lastMessage->message;
                // $listing['created_at'] = $message->created_at;
                $messageArr[] = $listing;
            }
            return response()->json(['status' => '1', 'message' => 'Data retrieved successfully','messages' => $messageArr]);
        }
    }

    public function rateDriver(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(),[
            'user_id'=>'required',
            'driver_id' => 'required',
            'rating' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else
        {
            $data = array(
                'user_id' => $saveArray['user_id'],
                'driver_id' => $saveArray['driver_id'],
                'rating' => $saveArray['rating']
             );  
             $rate_driver = Rating::insert($data);
             if ($rate_driver){
                return response()->json(['status' => '1', 'message' => 'Ratings succesfully']);
            }
            else
            {
                return response()->json(['status' => '1', 'message' => 'something went wrong']);
            }    
        }
    }

    public function notifications($type,$deviceIds){
        // if ($type == 'A') {
                $SERVER_API_KEY = 'AAAAU-sQ800:APA91bHPuZc9oB3AB0POkjbjNQTJaLS5Xv2dsbur8Fzbjg6bB8cq2IrVj0YFCEykoytE8rpowiDnhPUdwjV9iqwoZ9pg8LUHWBikePtU680Iu6Lb-xVJZHnj-5uQiKlXrDkCau_a2n3C';
                $message = "Booking Notification";
                        $data = [
                            "registration_ids" => array($deviceIds['device_id']),
                            "notification" => [
                                'id'=> $user->id,
                                // "title" => 'Appointment today with '.$salonData->name.'',
                                "title" => 'Truck Booking',
                                'type'=>'Appointment',
                                "body" => $message,  
                            ]
                        ];
                        $dataString = json_encode($data);
                    
                        $headers = [
                            'Authorization: key=' . $SERVER_API_KEY,
                            'Content-Type: application/json',
                        ];
                    
                        $ch = curl_init();
                      
                        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                               
                        $result = curl_exec($ch);
                        if ($result === FALSE){
                            die('Curl failed: ' . curl_error($ch));
                        }
                        curl_close($ch);
                        // print_r($result);die();
                        return $result;
            // }else{
            //     $result='';
            //     $body['aps'] = array(
            //         'alert' => [
            //          "body" => $message['message'],
            //          "title" => $message['title']
            //         ],
            //         'noti_for'=> $message,
            //         'sound' => 'default',
            //         'badge' => 0
            //     );
            //     $payload = json_encode($body);
            //     $ctx = stream_context_create();
            //     $passphrase = '';
            //     // stream_context_set_option($ctx, 'ssl', 'local_cert', 'public/pem/nameOfFile.pem');
            //     // stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            //     // $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 4, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            
            //     if($fp){
            //         $message = chr(0) . pack('n', 32) . pack('H*', $deviceIdArr[0]) . pack('n', strlen($payload)) . $payload;
            //         $result = fwrite($fp, $message, strlen($message)); 
            //         fclose($fp);
            //     }
            //     return $result;
            // }
    }

    public function getbookinglist(Request $request){
        $bookedlist = Book::where('user_id',$request->user_id)->get();
        if ($bookedlist)
        {
           $bookedlistArr = [];
       foreach($bookedlist as $getlist){
        $getUser = UserDetail::where('id',$getlist['user_id'])->first();
        $getTruck = Truck::where('id',$getlist['truck_id'])->first();
        $driver = Driver::where('vehicle_no', '1')->first();
               $listing['user_id'] = $getlist['user_id'];
               $listing['user'] = $getUser;
               $listing['truck_id'] = $getTruck['id'];
               $listing['truck_name'] = 'Mahindra';
               $listing['transport'] = 'Mahindra';
               $listing['km'] = '250';
               $listing['phone '] = '01784154285';
               $listing['payload'] = $getTruck['payload'];
               $listing['cargobox'] =$getTruck['cargobox'];
               $listing['type'] =$getTruck['type'];
               $listing['truck_type'] =$getTruck['truck_type'];
               $listing['vehicle_no'] =$getTruck['vehicle_no'];
               $listing['driver_name'] =$driver['name'];
               $listing['book_from'] =$getlist['book_from'];
               $listing['book_to'] =$getlist['book_to'];
               $listing['status'] =$getlist['status'];
               $bookedlistArr[] = $listing;
            }
            return response()->json(['status' => '1', 'message' => 'Data retrieved successfully','Booked_truck' => $bookedlistArr]);
        }
            else
            {
                return response()->json(['status' => '0', 'message' => 'no data', ]);
            }
    }

    public function follow(Request $request)
    {

        $saveArray = $request->all();
        $validator = Validator::make($request->all(),[
            'user_id'=>'required',
            'driver_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else
        {
            $checkfollow = Follow::where('user_id',$saveArray['user_id'])
            ->where('driver_id',$saveArray['driver_id'])
            ->first();
            if($checkfollow){
                $unfollow = Follow::where('user_id',$saveArray['user_id'])
                ->where('driver_id',$saveArray['driver_id'])
                ->delete();
                return response()->json(['status' => '1', 'message' => 'unfollowed succesfully']);
            }else{
                $data1 = array(
                    'user_id' => $saveArray['user_id'],
                    'driver_id' => $saveArray['driver_id'],
                );
                $followdriver = Follow::insert($data1);
                if ($followdriver){
                    return response()->json(['status' => '1', 'message' => 'Follow succesfully']);
                }
                else
                {
                    return response()->json(['status' => '1', 'message' => 'something went wrong']);
                }
            }
        }
    }


    public function favourite(Request $request)
    {
        $saveArray = $request->all();
        $validator = Validator::make($request->all(),[
            'user_id'=>'required',
            'driver_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->successStatus);
        }
        else
        {
            $checkfollow = Favourite::where('user_id',$saveArray['user_id'])
            ->where('driver_id',$saveArray['driver_id'])
            ->first();
            if($checkfollow){
                $unfollow = Favourite::where('user_id',$saveArray['user_id'])
                ->where('driver_id',$saveArray['driver_id'])
                ->delete();
                return response()->json(['status' => '1', 'message' => 'unlike succesfully']);
            }else{
                $data1 = array(
                    'user_id' => $saveArray['user_id'],
                    'driver_id' => $saveArray['driver_id'],
                );
                $followdriver = Favourite::insert($data1);
                if ($followdriver){
                    return response()->json(['status' => '1', 'message' => 'liked succesfully']);
                }
                else
                {
                    return response()->json(['status' => '1', 'message' => 'something went wrong']);
                }
            }
        }
    }

    public function changePassword(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'old_password' => 'required',
            'new_password' => 'required',
          ]);
          if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
          }
          elseif ($request->old_password == $request->new_password) {
            return response()->json(['message'=>'New password is same as old password','status'=>'0'], $this->successStatus);
          }
          else{
          $existingUser = UserDetail::where('id', $request->user_id)->first();
              if(Hash::check($request->old_password, $existingUser->password)){
                $password_en =  Hash::make($request->new_password);
                $update = UserDetail::where(['id'=>$existingUser->id])->update(['password' => $password_en]);
                // echo $update;
                if($update){
                  return response()->json([
                        'status'=>'1',
                        'message'=>'Password changed Successfully'
                      ], $this->successStatus);
                }
                else{
                  return response()->json([
                        'status'=>'0',
                        'message'=>'Error on changing password'
                      ], $this->successStatus);
                }
              }
              else{
                return response()->json([
                    'status'=>'0',
                    'message'=>'Old password is incorrect'
                ], $this->successStatus);
            }
        }
    }
      
    public function resetPasssword(Request $request){
        $saveArray = $request->all();
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'new_password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }
        else{
            $existingUser = UserDetail::where('id', $request->user_id)->first();
            $password_en =  Hash::make($request->new_password);
            $update = UserDetail::where(['id'=>$existingUser->id])->update(['password' => $password_en]);
            if($update){
                return response()->json([
                    'status'=>'1',
                    'message'=>'Password changed Successfully'
                ], $this->successStatus);
            }
            else{
                return response()->json([
                    'status'=>'0',
                    'message'=>'Error on changing password'
                ], $this->successStatus);
            }
        }
    }

    public function search(Request $request){
        $saveArray = $request->all();
        $token = $request->user_id;
        $validator = Validator::make($request->all(), [
            'search' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'0'], $this->badrequest);
        }else{
            $existingUser = UserDetail::where('id',$token)->first();
            if ($existingUser) {
                $searchArr = [];
                $trucks = Truck::where('type', 'LIKE', '%' . $request->search . '%')
                ->orWhere('payload', 'LIKE', '%' . $request->search . '%')
                ->orWhere('truck_type', 'LIKE', '%' . $request->search . '%')
                ->orWhere('vehicle', 'LIKE', '%' . $request->search . '%')
                ->orWhere('driver', 'LIKE', '%' . $request->search . '%')
                ->get();
                foreach($trucks AS $user){
                    $image = asset('/user_images/' . $user['images']);
                    $slist['id'] = (String)$user['id'];
                    $slist['payload'] = $user->payload;
                    $slist['type'] = $request->type;
                    $slist['truck_type'] = $request->truck_type;
                    $slist['driver'] = $request->driver;
                    $slist['vehicle'] = $request->vehicle;
                    $slist['image'] = $image;
                    $searchArr[] = $slist;
                }
                return response()->json([
                    'status'=>'1',
                    'message'=>'data fetched successfully',
                    'searchArray'=>$searchArr
                ], $this->successStatus);
            }else{
                return response()->json(['message'=>'User not exists','status'=>'0'
                ], $this->badrequest);
            }
        }
    }
    
}
