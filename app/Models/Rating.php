<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Truck
 * @package App\Models
 * @version July 17, 2021, 12:58 pm UTC
 *
 * @property string $payload
 * @property string $cargobox
 * @property string $type
 * @property string $driver
 * @property number $vehicle
 * @property string $images
 */
class Rating extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'ratings';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'driver_id',
        'rating'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'driver_id' => 'integer',
        'rating' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'payload' => 'required',
        // 'cargobox' => 'required',
        // 'type' => 'required',
        // 'driver' => 'required',
        // 'vehicle' => 'required',
        // 'images' => 'required'
    ];

    
}
