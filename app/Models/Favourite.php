<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class UserDetail
 * @package App\Models
 * @version July 17, 2021, 1:05 pm UTC
 *
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $gender
 * @property integer $phone_no
 * @property string $status
 */
class Favourite extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'favourite';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'driver_id',
        'current_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'driver_id' => 'integer',
        'current_status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'driver_id' => 'required',
        'current_status' => 'required'
    ];

    
}
