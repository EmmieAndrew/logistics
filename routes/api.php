<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerAPIController;
//use App\Http\Controllers\API\TruckAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [CustomerAPIController::class, 'login']);
Route::post('/socialLogin', [CustomerAPIController::class, 'socialLogin']);
Route::post('/register', [CustomerAPIController::class, 'register']);
Route::post('/forgotPassword', [CustomerAPIController::class, 'forgotPassword']);
Route::post('/changePassword', [CustomerAPIController::class, 'changePassword']);
Route::post('/resetpassword', [CustomerAPIController::class, 'resetPasssword']);
Route::get('/getOwnProfile', [CustomerAPIController::class, 'getOwnProfile']);
Route::post('/editProfile', [CustomerAPIController::class, 'editProfile']);
Route::post('/favourite', [CustomerAPIController::class, 'favourite']);
Route::post('/follow', [CustomerAPIController::class, 'follow']);
Route::post('/forgotpass', [CustomerAPIController::class, 'forgot_password']);
Route::post('/sendMessage', [CustomerAPIController::class, 'sendMessage']);
Route::post('/getMessages', [CustomerAPIController::class, 'getMessages']);
Route::post('/recentChats', [CustomerAPIController::class, 'recentChats']);
Route::post('/rateDriver', [CustomerAPIController::class, 'rateDriver']);
Route::get('/driverInfo', [CustomerAPIController::class, 'driverInfo']);
//Truck API's
Route::get('/truck', [CustomerAPIController::class, 'getlist']);
Route::post('/book', [CustomerAPIController::class, 'addbooking']);
Route::get('/booklist', [CustomerAPIController::class, 'getbookinglist']);
Route::post('/search', [CustomerAPIController::class, 'search']);